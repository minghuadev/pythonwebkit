Test IndexedDB: mutating records with a r/w cursor updates indexes on those records

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB;
PASS indexedDB == null is false
IDBDatabaseException = window.IDBDatabaseException || window.webkitIDBDatabaseException;
PASS IDBDatabaseException == null is false
IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
PASS IDBTransaction == null is false
indexedDB.open(name, description)
openSuccess():
db = event.target.result
firstValue = 'hi';
secondValue = 'bye';
objectStoreInfo = [
        { name: '1', options: {}, key: 1,
          entry: { data: firstValue } },
        { name: '2', options: { keyPath: 'foo' },
          entry: { foo: 1, data: firstValue } },
        { name: '3', options: { autoIncrement: true },
          entry: { data: firstValue } },
        { name: '4', options: { keyPath: 'foo', autoIncrement: true },
          entry: { data: firstValue } },
    ];
i = 0;
info = objectStoreInfo[i];
request = db.setVersion('1')
setupObjectStoreAndCreateIndex():
Deleted all object stores.
objectStore = db.createObjectStore(info.name, info.options);
index = objectStore.createIndex('data_index', 'data', { unique: false });
uniqueIndex = objectStore.createIndex('unique_data_index', 'data', { unique: true });
request = objectStore.add(info.entry, info.key);
request = objectStore.openCursor();
cursor = request.result;
value = cursor.value;
value.data = secondValue;
request = cursor.update(value);
request = index.get(secondValue);
PASS value.data is event.target.result.data
request = uniqueIndex.get(secondValue);
PASS value.data is event.target.result.data
i++;
info = objectStoreInfo[i];
request = db.setVersion('1')
setupObjectStoreAndCreateIndex():
Deleted all object stores.
objectStore = db.createObjectStore(info.name, info.options);
index = objectStore.createIndex('data_index', 'data', { unique: false });
uniqueIndex = objectStore.createIndex('unique_data_index', 'data', { unique: true });
request = objectStore.add(info.entry);
request = objectStore.openCursor();
cursor = request.result;
value = cursor.value;
value.data = secondValue;
request = cursor.update(value);
request = index.get(secondValue);
PASS value.data is event.target.result.data
request = uniqueIndex.get(secondValue);
PASS value.data is event.target.result.data
i++;
info = objectStoreInfo[i];
request = db.setVersion('1')
setupObjectStoreAndCreateIndex():
Deleted all object stores.
objectStore = db.createObjectStore(info.name, info.options);
index = objectStore.createIndex('data_index', 'data', { unique: false });
uniqueIndex = objectStore.createIndex('unique_data_index', 'data', { unique: true });
request = objectStore.add(info.entry);
request = objectStore.openCursor();
cursor = request.result;
value = cursor.value;
value.data = secondValue;
request = cursor.update(value);
request = index.get(secondValue);
PASS value.data is event.target.result.data
request = uniqueIndex.get(secondValue);
PASS value.data is event.target.result.data
i++;
info = objectStoreInfo[i];
request = db.setVersion('1')
setupObjectStoreAndCreateIndex():
Deleted all object stores.
objectStore = db.createObjectStore(info.name, info.options);
index = objectStore.createIndex('data_index', 'data', { unique: false });
uniqueIndex = objectStore.createIndex('unique_data_index', 'data', { unique: true });
request = objectStore.add(info.entry);
request = objectStore.openCursor();
cursor = request.result;
value = cursor.value;
value.data = secondValue;
request = cursor.update(value);
request = index.get(secondValue);
PASS value.data is event.target.result.data
request = uniqueIndex.get(secondValue);
PASS value.data is event.target.result.data
i++;
PASS successfullyParsed is true

TEST COMPLETE

