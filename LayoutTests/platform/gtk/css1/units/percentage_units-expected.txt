layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584 [bgcolor=#CCCCCC]
      RenderBlock {P} at (0,0) size 784x19
        RenderText {#text} at (0,0) size 355x19
          text run at (0,0) width 355: "The style declarations which apply to the text below are:"
      RenderBlock {PRE} at (0,35) size 784x75
        RenderText {#text} at (0,0) size 504x75
          text run at (0,0) width 224: ".zero { background: yellow }"
          text run at (224,0) width 0: " "
          text run at (0,15) width 504: ".one { margin-left: 25%; margin-right: 25%; background: white }"
          text run at (504,15) width 0: " "
          text run at (0,30) width 496: ".two { margin-left: 50%; margin-right: 0%; background: white }"
          text run at (496,30) width 0: " "
          text run at (0,45) width 208: ".three {margin-left: 25%;}"
          text run at (208,45) width 0: " "
          text run at (0,60) width 0: " "
      RenderBlock {HR} at (0,123) size 784x2 [border: (1px inset #000000)]
      RenderBlock {DIV} at (0,141) size 784x130 [bgcolor=#FFFF00]
        RenderBlock {DIV} at (196,0) size 392x57 [bgcolor=#FFFFFF]
          RenderBlock {P} at (0,0) size 392x57
            RenderText {#text} at (0,0) size 326x57
              text run at (0,0) width 326: "This paragraph should be centered within its yellow"
              text run at (0,19) width 323: "containing block and its width should be half of the"
              text run at (0,38) width 109: "containing block."
        RenderBlock {DIV} at (392,73) size 392x57 [bgcolor=#FFFFFF]
          RenderBlock {P} at (0,0) size 392x57
            RenderText {#text} at (0,0) size 353x57
              text run at (0,0) width 353: "This paragraph should be right-aligned within its yellow"
              text run at (0,19) width 323: "containing block and its width should be half of the"
              text run at (0,38) width 109: "containing block."
      RenderBlock {P} at (196,287) size 588x38
        RenderText {#text} at (0,0) size 549x38
          text run at (0,0) width 549: "This paragraph should have a left margin of 25% the width of its parent element, which"
          text run at (0,19) width 357: "should require some extra text in order to test effectively."
      RenderTable {TABLE} at (0,341) size 784x221 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 782x219
          RenderTableRow {TR} at (0,0) size 782x27
            RenderTableCell {TD} at (0,0) size 782x27 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=2]
              RenderInline {STRONG} at (0,0) size 163x19
                RenderText {#text} at (4,4) size 163x19
                  text run at (4,4) width 163: "TABLE Testing Section"
          RenderTableRow {TR} at (0,27) size 782x192
            RenderTableCell {TD} at (0,109) size 12x27 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (4,4) size 4x19
                text run at (4,4) width 4: " "
            RenderTableCell {TD} at (12,27) size 770x192 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderBlock {DIV} at (4,4) size 762x130 [bgcolor=#FFFF00]
                RenderBlock {DIV} at (190,0) size 382x57 [bgcolor=#FFFFFF]
                  RenderBlock {P} at (0,0) size 382x57
                    RenderText {#text} at (0,0) size 326x57
                      text run at (0,0) width 326: "This paragraph should be centered within its yellow"
                      text run at (0,19) width 323: "containing block and its width should be half of the"
                      text run at (0,38) width 109: "containing block."
                RenderBlock {DIV} at (381,73) size 381x57 [bgcolor=#FFFFFF]
                  RenderBlock {P} at (0,0) size 381x57
                    RenderText {#text} at (0,0) size 353x57
                      text run at (0,0) width 353: "This paragraph should be right-aligned within its yellow"
                      text run at (0,19) width 323: "containing block and its width should be half of the"
                      text run at (0,38) width 109: "containing block."
              RenderBlock {P} at (194,150) size 572x38
                RenderText {#text} at (0,0) size 549x38
                  text run at (0,0) width 549: "This paragraph should have a left margin of 25% the width of its parent element, which"
                  text run at (0,19) width 357: "should require some extra text in order to test effectively."
