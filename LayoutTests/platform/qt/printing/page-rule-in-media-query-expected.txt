layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (1,1) size 798x78
  RenderBlock {HTML} at (1,1) size 798x78
    RenderBody {BODY} at (1,1) size 796x76
      RenderBlock {DIV} at (1,0) size 794x76 [border: (1px solid #000000)]
        RenderBlock (anonymous) at (1,1) size 792x22
          RenderText {#text} at (0,0) size 42x22
            text run at (0,0) width 42: "DIV 1"
        RenderBlock {DIV} at (2,24) size 790x50 [border: (1px solid #000000)]
          RenderBlock (anonymous) at (1,1) size 788x22
            RenderText {#text} at (0,0) size 42x22
              text run at (0,0) width 42: "DIV 2"
          RenderBlock {DIV} at (2,24) size 786x24 [border: (1px solid #000000)]
            RenderText {#text} at (1,1) size 344x22
              text run at (1,1) width 344: "The margins of divs should remain 1px in printing."
