layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x128
  RenderBlock {HTML} at (0,0) size 800x128
    RenderBody {BODY} at (8,16) size 784x104 [color=#0000FF]
      RenderBlock {P} at (0,0) size 784x22
        RenderText {#text} at (0,0) size 693x22
          text run at (0,0) width 693: "There should be three solid bars of colour below, each longer than the last, each of the colour specified."
      RenderBlock {DIV} at (0,38) size 784x22
        RenderText {#text} at (0,0) size 40x22
          text run at (0,0) width 40: "Blue: "
        RenderInline {SPAN} at (0,0) size 15x15
          RenderText {#text} at (40,7) size 15x15
            text run at (40,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15
          RenderText {#text} at (55,7) size 15x15
            text run at (55,7) width 15: "X"
        RenderImage {IMG} at (70,7) size 15x15 [color=#FF0000]
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {DIV} at (0,60) size 784x22
        RenderText {#text} at (0,0) size 60x22
          text run at (0,0) width 60: "Orange: "
        RenderInline {SPAN} at (0,0) size 15x15 [color=#FFA500]
          RenderText {#text} at (60,7) size 15x15
            text run at (60,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#FFA500]
          RenderText {#text} at (75,7) size 15x15
            text run at (75,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#FFA500]
          RenderText {#text} at (90,7) size 15x15
            text run at (90,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#FFA500]
          RenderText {#text} at (105,7) size 15x15
            text run at (105,7) width 15: "X"
        RenderImage {IMG} at (120,7) size 15x15 [color=#FF0000]
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {DIV} at (0,82) size 784x22
        RenderText {#text} at (0,0) size 44x22
          text run at (0,0) width 44: "Lime: "
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (44,7) size 15x15
            text run at (44,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (59,7) size 15x15
            text run at (59,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (74,7) size 15x15
            text run at (74,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (89,7) size 15x15
            text run at (89,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (104,7) size 15x15
            text run at (104,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (119,7) size 15x15
            text run at (119,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (134,7) size 15x15
            text run at (134,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (149,7) size 15x15
            text run at (149,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (164,7) size 15x15
            text run at (164,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (179,7) size 15x15
            text run at (179,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (194,7) size 15x15
            text run at (194,7) width 15: "X"
        RenderInline {SPAN} at (0,0) size 15x15 [color=#00FF00]
          RenderText {#text} at (209,7) size 15x15
            text run at (209,7) width 15: "X"
        RenderImage {IMG} at (224,7) size 15x15 [color=#FF0000]
        RenderText {#text} at (0,0) size 0x0
