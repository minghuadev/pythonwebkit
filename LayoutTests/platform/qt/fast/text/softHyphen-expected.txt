layer at (0,0) size 784x1108
  RenderView at (0,0) size 784x600
layer at (0,0) size 784x1108
  RenderBlock {HTML} at (0,0) size 784x1108
    RenderBody {BODY} at (8,16) size 768x1076
      RenderBlock {P} at (0,0) size 768x22
        RenderText {#text} at (0,0) size 458x22
          text run at (0,0) width 458: "In all of the following, there should not be a hyphen before \x{201C}lorem\x{201D}."
      RenderBlock {P} at (0,38) size 768x22
        RenderText {#text} at (0,0) size 20x22
          text run at (0,0) width 20: "Do"
        RenderBlock (floating) {SPAN} at (754,0) size 14x24 [border: (1px solid #FF0000)]
          RenderText {#text} at (1,1) size 12x22
            text run at (1,1) width 12: "X"
        RenderText {#text} at (20,0) size 84x22
          text run at (20,0) width 84: "\x{AD}lorem ipsum"
      RenderBlock {P} at (0,76) size 768x22
        RenderBlock (floating) {SPAN} at (754,0) size 14x24 [border: (1px solid #FF0000)]
          RenderText {#text} at (1,1) size 12x22
            text run at (1,1) width 12: "X"
        RenderText {#text} at (0,0) size 84x22
          text run at (0,0) width 84: "lorem ipsum"
      RenderBlock {P} at (0,114) size 768x24
        RenderText {#text} at (0,1) size 20x22
          text run at (0,1) width 20: "Do"
        RenderText {#text} at (34,1) size 84x22
          text run at (34,1) width 84: "\x{AD}lorem ipsum"
      RenderBlock {P} at (0,154) size 768x22
        RenderText {#text} at (0,0) size 20x22
          text run at (0,0) width 20: "Do"
        RenderText {#text} at (20,0) size 84x22
          text run at (20,0) width 84: "\x{AD}lorem ipsum"
      RenderBlock (anonymous) at (0,192) size 768x22
        RenderText {#text} at (0,0) size 20x22
          text run at (0,0) width 20: "Do"
      RenderBlock {P} at (0,230) size 768x22
        RenderText {#text} at (0,0) size 84x22
          text run at (0,0) width 84: "lorem ipsum"
      RenderBlock {P} at (0,268) size 768x44
        RenderText {#text} at (0,0) size 20x22
          text run at (0,0) width 20: "Do"
        RenderBR {BR} at (20,16) size 0x0
        RenderText {#text} at (0,22) size 84x22
          text run at (0,22) width 84: "lorem ipsum"
      RenderBlock {P} at (0,328) size 768x22
        RenderText {#text} at (0,0) size 20x22
          text run at (0,0) width 20: "Do"
        RenderInline {SPAN} at (0,0) size 39x22
          RenderText {#text} at (20,0) size 39x22
            text run at (20,0) width 39: "\x{AD}lorem"
        RenderText {#text} at (59,0) size 45x22
          text run at (59,0) width 45: " ipsum"
      RenderBlock {P} at (0,366) size 768x22
        RenderText {#text} at (0,0) size 12x22
          text run at (0,0) width 12: "D"
        RenderInline {SPAN} at (0,0) size 8x22
          RenderText {#text} at (12,0) size 8x22
            text run at (12,0) width 8: "o"
        RenderText {#text} at (20,0) size 84x22
          text run at (20,0) width 84: "\x{AD}lorem ipsum"
      RenderBlock {P} at (0,404) size 768x22
        RenderText {#text} at (0,0) size 24x22
          text run at (0,0) width 24: "Do "
        RenderInline {SPAN} at (0,0) size 39x22
          RenderText {#text} at (24,0) size 39x22
            text run at (24,0) width 39: "\x{AD}lorem"
        RenderText {#text} at (63,0) size 45x22
          text run at (63,0) width 45: " ipsum"
      RenderBlock {P} at (0,442) size 768x22
        RenderText {#text} at (0,0) size 12x22
          text run at (0,0) width 12: "D"
        RenderInline {SPAN} at (0,0) size 12x22
          RenderText {#text} at (12,0) size 12x22
            text run at (12,0) width 12: "o "
        RenderText {#text} at (24,0) size 84x22
          text run at (24,0) width 84: "\x{AD}lorem ipsum"
      RenderBlock {P} at (0,480) size 768x22
        RenderText {#text} at (0,0) size 24x22
          text run at (0,0) width 24: "Do "
        RenderInline {SPAN} at (0,0) size 39x22
          RenderText {#text} at (24,0) size 39x22
            text run at (24,0) width 39: "\x{AD}lorem"
        RenderText {#text} at (63,0) size 45x22
          text run at (63,0) width 45: " ipsum"
      RenderBlock {P} at (0,518) size 768x22
        RenderText {#text} at (0,0) size 12x22
          text run at (0,0) width 12: "D"
        RenderInline {SPAN} at (0,0) size 12x22
          RenderText {#text} at (12,0) size 12x22
            text run at (12,0) width 12: "o "
        RenderText {#text} at (24,0) size 84x22
          text run at (24,0) width 84: "\x{AD}lorem ipsum"
      RenderBlock {P} at (0,556) size 768x22
        RenderText {#text} at (0,0) size 67x22
          text run at (0,0) width 67: "Do \x{AD} lorem"
      RenderBlock {P} at (0,594) size 768x22
        RenderText {#text} at (0,0) size 63x22
          text run at (0,0) width 63: "Do\x{AD} \x{AD}lorem"
      RenderBlock {P} at (0,632) size 768x22
        RenderText {#text} at (0,0) size 67x22
          text run at (0,0) width 24: "Do "
          text run at (24,0) width 43: "\x{AD}lorem"
      RenderBlock {P} at (0,670) size 768x22
        RenderText {#text} at (0,0) size 67x22
          text run at (0,0) width 28: "Do\x{AD} "
          text run at (28,0) width 39: "lorem"
      RenderBlock {P} at (0,708) size 768x22
        RenderText {#text} at (0,0) size 71x22
          text run at (0,0) width 32: "Do \x{AD} "
          text run at (32,0) width 39: "lorem"
      RenderBlock {P} at (0,746) size 768x22
        RenderText {#text} at (0,0) size 71x22
          text run at (0,0) width 24: "Do "
          text run at (24,0) width 47: "\x{AD} lorem"
      RenderBlock {P} at (0,784) size 768x22
        RenderText {#text} at (0,0) size 20x22
          text run at (0,0) width 20: "Do"
        RenderInline {SPAN} at (0,0) size 0x22
        RenderText {#text} at (20,0) size 84x22
          text run at (20,0) width 84: "\x{AD}lorem ipsum"
      RenderBlock {P} at (0,822) size 768x22
        RenderText {#text} at (0,0) size 104x22
          text run at (0,0) width 104: "Do\x{AD}\x{AD}lorem ipsum"
      RenderBlock {P} at (0,860) size 768x22
        RenderInline {SPAN} at (0,0) size 20x22
          RenderText {#text} at (0,0) size 20x22
            text run at (0,0) width 20: "Do\x{AD}"
        RenderText {#text} at (20,0) size 84x22
          text run at (20,0) width 84: "\x{AD}lorem ipsum"
      RenderBlock {P} at (0,898) size 768x22
        RenderText {#text} at (0,0) size 20x22
          text run at (0,0) width 20: "Do\x{AD}"
        RenderInline {SPAN} at (0,0) size 84x22
          RenderText {#text} at (20,0) size 84x22
            text run at (20,0) width 84: "\x{AD}lorem ipsum"
      RenderBlock {P} at (0,936) size 768x22
        RenderInline {SPAN} at (0,0) size 20x22
          RenderText {#text} at (0,0) size 20x22
            text run at (0,0) width 20: "Do\x{AD}\x{AD}"
        RenderText {#text} at (20,0) size 84x22
          text run at (20,0) width 84: "lorem ipsum"
      RenderBlock {P} at (0,974) size 768x22
        RenderText {#text} at (0,0) size 262x22
          text run at (0,0) width 262: "The following pair should be the same:"
      RenderBlock {P} at (0,1012) size 768x24
        RenderText {#text} at (0,1) size 16x22
          text run at (0,1) width 16: "W"
        RenderBlock {SPAN} at (16,0) size 14x24 [border: (1px solid #FF0000)]
          RenderText {#text} at (1,1) size 12x22
            text run at (1,1) width 12: "X"
        RenderText {#text} at (30,1) size 12x22
          text run at (30,1) width 12: "Y"
      RenderBlock {P} at (0,1052) size 768x24
        RenderText {#text} at (0,1) size 16x22
          text run at (0,1) width 16: "W"
        RenderBlock {SPAN} at (16,0) size 14x24 [border: (1px solid #FF0000)]
          RenderText {#text} at (1,1) size 12x22
            text run at (1,1) width 12: "X"
        RenderText {#text} at (30,1) size 12x22
          text run at (30,1) width 12: "Y"
layer at (28,130) size 14x24
  RenderBlock (relative positioned) {SPAN} at (20,0) size 14x24 [border: (1px solid #FF0000)]
    RenderText {#text} at (1,1) size 12x22
      text run at (1,1) width 12: "X"
layer at (770,170) size 14x24
  RenderBlock (positioned) {SPAN} at (770,170) size 14x24 [border: (1px solid #FF0000)]
    RenderText {#text} at (1,1) size 12x22
      text run at (1,1) width 12: "X"
