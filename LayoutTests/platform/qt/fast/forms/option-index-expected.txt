layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock (anonymous) at (0,0) size 784x44
        RenderText {#text} at (0,0) size 755x44
          text run at (0,0) width 755: "This test makes sure we don't crash when trying to get the index of an option element that has no corresponding"
          text run at (0,22) width 96: "select element."
      RenderBlock {DIV} at (0,44) size 784x44
        RenderText {#text} at (0,0) size 87x22
          text run at (0,0) width 87: "Test Passed. "
        RenderBR {BR} at (87,16) size 0x0
        RenderText {#text} at (0,22) size 394x22
          text run at (0,22) width 394: "Index for option element with no corresponding select is: 0"
