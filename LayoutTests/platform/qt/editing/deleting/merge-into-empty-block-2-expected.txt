EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 2 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldDeleteDOMRange:range from 0 of DIV > DIV > BODY > HTML > #document to 0 of LI > UL > DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 760x44
          text run at (0,0) width 760: "When a user puts the caret at the very beginning a list and hits delete into an empty line, the list should just move"
          text run at (0,22) width 22: "up."
      RenderBlock {DIV} at (0,60) size 784x22
        RenderBlock {UL} at (0,0) size 784x22
          RenderListItem {LI} at (40,0) size 744x22
            RenderListMarker at (-18,0) size 7x22: bullet
            RenderInline {SPAN} at (0,0) size 21x22
              RenderText {#text} at (0,0) size 21x22
                text run at (0,0) width 21: "foo"
caret: position 0 of child 0 {#text} of child 0 {SPAN} of child 0 {LI} of child 0 {UL} of child 2 {DIV} of body
