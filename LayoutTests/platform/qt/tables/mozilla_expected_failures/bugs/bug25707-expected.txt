layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 765x44
          text run at (0,0) width 765: "The table below should be sized so that each word of text wraps to its own line, and if the font size is not too large,"
          text run at (0,22) width 320: "then the table will not be wider than the screen."
      RenderBlock {P} at (0,60) size 784x110
        RenderText {#text} at (0,0) size 760x110
          text run at (0,0) width 756: "Previously, the table would be incredibly wide, wide enough so that the entire paragraph of text was on one line."
          text run at (0,22) width 760: "That was because the \"maxElementSize\" of the cell was including margins calculated from the desired size of the"
          text run at (0,44) width 677: "text (in this case, 19x the desired width of the paragraph). The fix was to calculate the margin for the"
          text run at (0,66) width 750: "\"maxElementSize\" of the cell using the \"maxElementSize\" of the contained block itself, which in this case is the"
          text run at (0,88) width 184: "maximum width of a word."
      RenderTable {TABLE} at (0,186) size 244x186 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 242x184
          RenderTableRow {TR} at (0,2) size 242x180
            RenderTableCell {TD} at (2,2) size 238x180 [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=1]
              RenderBlock {P} at (224,2) size 12x176
                RenderText {#text} at (0,0) size 34x176
                  text run at (0,0) width 30: "This"
                  text run at (0,22) width 10: "is"
                  text run at (0,44) width 34: "some"
                  text run at (0,66) width 29: "text."
                  text run at (0,88) width 30: "This"
                  text run at (0,110) width 10: "is"
                  text run at (0,132) width 34: "some"
                  text run at (0,154) width 29: "text."
